angular.module("Trello", [])
    .controller("TrelloController", ['$scope', function ($scope) {
        $scope.title = "Trello";
        $scope.editToDo = "";
        $scope.editDoing = "";
        $scope.editDone = "";
        $scope.editDropped = "";
        $scope.toDo = [];
        $scope.doing = [];
        $scope.done = [];
        $scope.dropped = [];
        $scope.toDoItem = "";
        $scope.doingItem = "";
        $scope.doneItem = "";
        $scope.droppedItem = "";

        $scope.add = function (item, flag) {
            if (item != "") {
                item = { description: item, edit: false };
                switch (flag) {
                    case 1:
                        $scope.toDo.push(item);
                        $scope.toDoItem = "";
                        break;
                    case 2:
                        $scope.doing.push(item);
                        $scope.doingItem = "";
                        break;
                    case 3:
                        $scope.done.push(item);
                        $scope.doneItem = "";
                        break;
                    case 4:
                        $scope.dropped.push(item);
                        $scope.droppedItem = "";
                        break;
                }
            }
        }

        $scope.exclude = function (item, flag) {
            var pos;
            switch (flag) {
                case 1:
                    pos = $scope.toDo.indexOf(item);
                    console.log(pos);
                    $scope.toDo.splice(pos, 1);
                    break;
                case 2:
                    pos = $scope.doing.indexOf(item);
                    $scope.doing.splice(pos, 1);
                    break;
                case 3:
                    pos = $scope.done.indexOf(item);
                    $scope.done.splice(pos, 1);
                    break;
                case 4:
                    pos = $scope.dropped.indexOf(item);
                    $scope.dropped.splice(pos, 1);
                    break;
            }
        }

        $scope.edit = function (item, editItem, flag) {
            var i, b = item.description;
            switch (flag) {
                case 1:
                    i = $scope.toDo.indexOf(item);
                    $scope.toDo[i].edit = !$scope.toDo[i].edit;
                    $scope.toDo[i].description = editItem;
                    $scope.editToDo = b;
                    break;
                case 2:
                    i = $scope.doing.indexOf(item);
                    $scope.doing[i].edit = !$scope.doing[i].edit;
                    $scope.doing[i].description = editItem;
                    $scope.editDoing = b;
                    break;
                case 3:
                    i = $scope.done.indexOf(item);
                    $scope.done[i].edit = !$scope.done[i].edit;
                    $scope.done[i].description = editItem;
                    $scope.editDone = b;
                    break;
                case 4:
                    i = $scope.dropped.indexOf(item);
                    $scope.dropped[i].edit = !$scope.dropped[i].edit;
                    $scope.dropped[i].description = editItem;
                    $scope.editDropped = b;
                    break;
            }
        }

        $scope.start = function (item) {
            var pos = $scope.toDo.indexOf(item);
            $scope.toDo.splice(pos, 1);
            $scope.doing.push(item);
        }

        $scope.finish = function (item) {
            var pos = $scope.doing.indexOf(item);
            $scope.doing.splice(pos, 1);
            $scope.done.push(item);
        }

        $scope.drop = function (item, flag) {
            switch (flag) {
                case 1:
                    var pos = $scope.toDo.indexOf(item);
                    $scope.toDo.splice(pos, 1);
                    break;
                case 2:
                    var pos = $scope.doing.indexOf(item);
                    $scope.doing.splice(pos, 1);
                    break;
            }
            $scope.dropped.push(item);
        }

        $scope.reset = function (item) {
            var pos = $scope.dropped.indexOf(item);
            $scope.dropped.splice(pos, 1);
            $scope.toDo.push(item);
        }

        $scope.goBack = function (item, flag) {
            var pos;
            switch (flag) {
                case 1:
                    pos = $scope.doing.indexOf(item);
                    $scope.doing.splice(pos, 1);
                    $scope.toDo.push(item);
                    break;
                case 2:
                    pos = $scope.done.indexOf(item);
                    $scope.done.splice(pos, 1);
                    $scope.doing.push(item);
                    break;
            }
        }
    }])
