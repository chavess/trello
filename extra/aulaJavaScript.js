var toDoListElement = document.querySelector(".to-do ul");
var toDoTextElement = document.querySelector(".to-do textarea");
var toDoExcludeButton = document.querySelector(".to-do button");
var doingListElement = document.querySelector(".doing ul");
var doingTextElement = document.querySelector(".doing textarea");
var doingExcludeButton = document.querySelector(".doing button");
var doneListElement = document.querySelector(".done ul");
var doneTextElement = document.querySelector(".done textarea");
var doneExcludeButton = document.querySelector(".done button");

var toDo = [];
var doing = [];
var done = [];

const button1 = document.querySelector('#botao1');
const button2 = document.querySelector('#botao2');
const button3 = document.querySelector('#botao3');
console.log("oi");

//to do{
function createToDo() {
  var toDoElement = document.createElement("li");
  var excludeButton = document.createElement("a");
  var excludeText = document.createTextNode("Excluir");
  var changeButton = document.createElement("a");
  var changeText = document.createTextNode("Alterar");

  toDoElement.setAttribute("draggable","true");
  toDoElement.setAttribute("ondragstart", "drag(event)");
  excludeButton.setAttribute("href", "#");
  excludeButton.setAttribute("class", "btn btn-danger btnOptions");
  excludeButton.setAttribute("onclick", `to_do_remove(${toDo.indexOf(item)})`);
  changeButton.setAttribute("href", "#");
  changeButton.setAttribute("class", "btn btn-info btnOptions");
  changeButton.setAttribute("onclick", `to_do_change(${toDo.indexOf(item)})`);

  toDoElement.appendChild(document.createTextNode(item));
  excludeButton.appendChild(excludeText);
  changeButton.appendChild(changeText);

  toDoListElement.appendChild(toDoElement);
  toDoListElement.appendChild(excludeButton);
  toDoListElement.appendChild(changeButton);
}

function loadToDoList() {
  toDoListElement.innerHTML = "";

  for (item of toDo) {
    createToDo();
  }
}

function to_do_add() {
  var toDoText = toDoTextElement.value;
  if (toDoText != "") {
    toDo.push(toDoText);
    toDoTextElement.value = "";

    loadToDoList();
  }
}

function to_do_remove(pos) {
  toDo.splice(pos, 1);

  loadToDoList();
}

function to_do_change(pos) {

  toDoListElement.innerHTML = "";

  for (item of toDo) {
    if (toDo.indexOf(item) != pos) createToDo();
    else {
      var toDoElement = document.createElement("textarea");
      var excludeButton = document.createElement("a");
      var excludeText = document.createTextNode("Excluir");
      var changeButton = document.createElement("a");
      var changeText = document.createTextNode("Alterar");

      toDoElement.setAttribute("class", "edit editToDo");
      excludeButton.setAttribute("href", "#");
      excludeButton.setAttribute("class", "btn btn-danger btnOptions");
      excludeButton.setAttribute("onclick", `to_do_remove(${toDo.indexOf(item)})`);
      changeButton.setAttribute("href", "#");
      changeButton.setAttribute("class", "btn btn-info btnOptions");
      changeButton.setAttribute("onclick", `to_do_save_change(${toDo.indexOf(item)})`);

      toDoElement.appendChild(document.createTextNode(item));
      excludeButton.appendChild(excludeText);
      changeButton.appendChild(changeText);

      toDoListElement.appendChild(toDoElement);
      toDoListElement.appendChild(excludeButton);
      toDoListElement.appendChild(changeButton);
    }
  }

}

function to_do_save_change(pos) {
  var editElement = document.querySelector(".editToDo");
  var editText = editElement.value;
  toDo[pos] = editText;
  loadToDoList();

}

//}
//doing{

function createDoing() {
  var doingElement = document.createElement("li");
  var excludeButton = document.createElement("a");
  var excludeText = document.createTextNode("Excluir");
  var changeButton = document.createElement("a");
  var changeText = document.createTextNode("Alterar");

  doingElement.setAttribute("draggable","true");
  doingElement.setAttribute("ondragstart", "drag(event)");
  excludeButton.setAttribute("href", "#");
  excludeButton.setAttribute("class", "btn btn-danger btnOptions");
  excludeButton.setAttribute("onclick", `doing_remove(${doing.indexOf(item)})`);
  changeButton.setAttribute("href", "#");
  changeButton.setAttribute("class", "btn btn-info btnOptions");
  changeButton.setAttribute("onclick", `doing_change(${doing.indexOf(item)})`);

  doingElement.appendChild(document.createTextNode(item));
  excludeButton.appendChild(excludeText);
  changeButton.appendChild(changeText);

  doingListElement.appendChild(doingElement);
  doingListElement.appendChild(excludeButton);
  doingListElement.appendChild(changeButton);

}

function loadDoingList() {
  doingListElement.innerHTML = "";

  for (item of doing) {
    createDoing();
  }
}

function doing_add() {
  var doingText = doingTextElement.value;
  if (doingText != "") {
    doing.push(doingText);
    doingTextElement.value = "";

    loadDoingList();
  }
}

function doing_remove(pos) {
  doing.splice(pos, 1);

  loadDoingList();
}

function doing_change(pos) {
  doingListElement.innerHTML = "";
  for (item of doing) {
    if (doing.indexOf(item) != pos) createDoing();
    else {
      var doingElement = document.createElement("textarea");
      var excludeButton = document.createElement("a");
      var excludeText = document.createTextNode("Excluir");
      var changeButton = document.createElement("a");
      var changeText = document.createTextNode("Alterar");

      doingElement.setAttribute("class", "edit editDoing");
      excludeButton.setAttribute("href", "#");
      excludeButton.setAttribute("class", "btn btn-danger btnOptions");
      excludeButton.setAttribute("onclick", `doing_remove(${doing.indexOf(item)})`);
      changeButton.setAttribute("href", "#");
      changeButton.setAttribute("class", "btn btn-info btnOptions");
      changeButton.setAttribute("onclick", `doing_save_change(${doing.indexOf(item)})`);

      doingElement.appendChild(document.createTextNode(item));
      excludeButton.appendChild(excludeText);
      changeButton.appendChild(changeText);

      doingListElement.appendChild(doingElement);
      doingListElement.appendChild(excludeButton);
      doingListElement.appendChild(changeButton);
    }
  }
}

function doing_save_change(pos) {
  var editElement = document.querySelector(".editDoing");
  var editText = editElement.value;
  doing[pos] = editText;
  loadDoingList();
}

//}
//done{

function createDone(){
  var doneElement = document.createElement("li");
  var excludeButton = document.createElement("a");
  var excludeText = document.createTextNode("Excluir");
  var changeButton = document.createElement("a");
  var changeText = document.createTextNode("Alterar");
  
  doneElement.setAttribute("draggable","true");
  doneElement.setAttribute("ondragstart", "drag(event)");
  excludeButton.setAttribute("href", "#");
  excludeButton.setAttribute("class", "btn btn-danger btnOptions");
  excludeButton.setAttribute("onclick", `done_remove(${done.indexOf(item)})`);
  changeButton.setAttribute("href", "#");
  changeButton.setAttribute("class", "btn btn-info btnOptions");
  changeButton.setAttribute("onclick", `done_change(${done.indexOf(item)})`);

  doneElement.appendChild(document.createTextNode(item));
  excludeButton.appendChild(excludeText);
  changeButton.appendChild(changeText);

  doneListElement.appendChild(doneElement);
  doneListElement.appendChild(excludeButton);
  doneListElement.appendChild(changeButton);
}

function loadDoneList() {
  doneListElement.innerHTML = "";

  for (item of done) {
    createDone();
  }
}

function done_add() {
  var doneText = doneTextElement.value;
  if (doneText != "") {
    done.push(doneText);
    doneTextElement.value = "";

    loadDoneList();
  }
}

function done_remove(pos) {
  done.splice(pos, 1);

  loadDoneList();
}

function done_change(pos) {
  doneListElement.innerHTML = "";
  for (item of done) {
    if (done.indexOf(item) != pos) createDone();
    else {
      var doneElement = document.createElement("textarea");
      var excludeButton = document.createElement("a");
      var excludeText = document.createTextNode("Excluir");
      var changeButton = document.createElement("a");
      var changeText = document.createTextNode("Alterar");

      doneElement.setAttribute("class", "edit editDone");
      excludeButton.setAttribute("href", "#");
      excludeButton.setAttribute("class", "btn btn-danger btnOptions");
      excludeButton.setAttribute("onclick", `done_remove(${done.indexOf(item)})`);
      changeButton.setAttribute("href", "#");
      changeButton.setAttribute("class", "btn btn-info btnOptions");
      changeButton.setAttribute("onclick", `done_save_change(${done.indexOf(item)})`);

      doneElement.appendChild(document.createTextNode(item));
      excludeButton.appendChild(excludeText);
      changeButton.appendChild(changeText);

      doneListElement.appendChild(doneElement);
      doneListElement.appendChild(excludeButton);
      doneListElement.appendChild(changeButton);
    }
  }
}

function done_save_change(pos) {
  var editElement = document.querySelector(".editDone");
  var editText = editElement.value;
  done[pos] = editText;
  loadDoneList();

}

//}

function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
}

button1.addEventListener('click', to_do_add);
button2.addEventListener('click', doing_add);
button3.addEventListener('click', done_add);
